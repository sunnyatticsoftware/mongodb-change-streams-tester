﻿using MongoDB.Bson;
using MongoDB.Driver;
// ReSharper disable StringLiteralTypo

const string connectionString = "mongodb://localhost:27017/?replicaSet=rs0";
var client = new MongoClient(connectionString);
var database = client.GetDatabase("testdb");
var collection = database.GetCollection<BsonDocument>("testcollection");

var options = new ChangeStreamOptions { FullDocument = ChangeStreamFullDocumentOption.UpdateLookup };

// Start monitoring the collection for changes.
var changeStream = collection.Watch(options);

// Start a new thread to insert a document into the collection.
var insertTask = Task.Run(() =>
{
	var document = new BsonDocument { { "name", "John Doe" }, { "age", 30 } };
	collection.InsertOne(document);
	Console.WriteLine("Inserted a new document.");
});

// Wait for a change in the collection.
foreach (var change in changeStream.ToEnumerable())
{
	Console.WriteLine($"Received a change: {change.FullDocument}");
	break; // Exit the loop after receiving the first change.
}

// Wait for the insert task to complete.
await insertTask;